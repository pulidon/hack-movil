import '../config.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
final JsonDecoder _decoder = new JsonDecoder();

class AuthService {


  Future login(String username, String password) async {
    Map<String,String> headers = {
      'Content-type' : 'application/json',
    };

    Map body = {
      'username': username,
      "password": password
    };

    String url = '$urlApp/login';
    return await http.post(url, headers: headers, body: jsonEncode(body))
    .then(_handleRequest);
  }

  Future register(String username, String password, String city, String country, String address, String idioma) async {
    Map<String,String> headers = {
      'Content-type' : 'application/json',
    };

    Map body = {
      'username': username,
      "password": password,
      "city": city,
      "country": country,
      "address": address,
      "idioma": idioma,
      "canales": [null]
    };

    print(body);

    String url = '$urlApp/create/user';
    return await http.post(url, headers: headers, body: jsonEncode(body))
    .then(_handleRequest);
  }

  _handleRequest(http.Response response) {
    final String res = response.body;
    final int statusCode = response.statusCode;
    print('Code: $statusCode Response $res');

    // if (statusCode == 401) {
    //   signOut();
    // }
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return _decoder.convert(res);
  }  
}