import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../config.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
final JsonDecoder _decoder = new JsonDecoder();

class ChannelService {
  FlutterSecureStorage storage = new FlutterSecureStorage();


  Future getChannels() async {
    // Read value 
    String token = await storage.read(key: 'token');
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Authorization': 'Token $token'
    };

    String url = '$urlApp/get_channel';
    return await http.get(url, headers: headers)
    .then(_handleRequest);
  }

  _handleRequest(http.Response response) {
    final String res = response.body;
    final int statusCode = response.statusCode;
    print('Code: $statusCode Response $res');

    // if (statusCode == 401) {
    //   signOut();
    // }
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return _decoder.convert(res);
  }  
}