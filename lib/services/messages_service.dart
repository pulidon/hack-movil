import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../config.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
final JsonDecoder _decoder = new JsonDecoder();

class MessagesService {
   FlutterSecureStorage storage = new FlutterSecureStorage();

  Future getMessagesByChannel(int id) async {
      String token = await storage.read(key: 'token');
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Authorization': 'Token $token'
    };

    Map body = {
      'canal': id,
    };

    String url = '$urlApp/get_messages';
    return await http.post(url, headers: headers, body: jsonEncode(body))
    .then(_handleRequest);
  }

  _handleRequest(http.Response response) {
    final String res = response.body;
    final int statusCode = response.statusCode;
    print('Code: $statusCode Response $res');

    // if (statusCode == 401) {
    //   signOut();
    // }
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return _decoder.convert(res);
  }  
}