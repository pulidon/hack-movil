import 'package:flutter/material.dart';
import 'package:flutter_connect/pages/root_page.dart';
import 'package:flutter_connect/pages/channels.dart';
import 'package:flutter_connect/pages/messages.dart';

void main() => runApp(FlutterConnect());

class FlutterConnect extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Roboto',
        primaryColor: Color.fromRGBO(48, 134, 26, 1.0),
      ),
      home: RootPage(),
      routes:  {
        '/channels': (BuildContext context) => ChannelsPage(),
        '/messages': (BuildContext context) => MessagesPage(),
      },
    );
  }
}