import 'package:flutter/material.dart';
import 'package:flutter_connect/services/auth_service.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

typedef ModeCallback = void Function(String mode);

class RegisterPage extends StatefulWidget {
  RegisterPage({this.changeMode});
  final ModeCallback changeMode;

  @override
  RegisterPageState createState() => new RegisterPageState();
}

class RegisterPageState extends State<RegisterPage> {
  final _formKey = new GlobalKey<FormState>();
  FlutterSecureStorage storage = new FlutterSecureStorage();
  AuthService _authService = new AuthService();
  TextEditingController _username = new TextEditingController(text: '');
  TextEditingController _password = new TextEditingController(text: '');
  TextEditingController _city = new TextEditingController(text: '');
  TextEditingController _country = new TextEditingController(text: '');
  TextEditingController _address = new TextEditingController(text: '');
  TextEditingController _language = new TextEditingController(text: '');
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  void saveData(token) async {
    await storage.write(key: 'token', value: token);
  }


  Widget _showBtnLogin() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0),
        child: FlatButton(
          child: Text('Ingresar'),
          onPressed: () => widget.changeMode("login"),
          // color: Color.fromRGBO(48, 134, 26, 1.0),
        ),
      ),
    );
  }

  void register() {
    setState(() => _isLoading = true);
    _authService.register(_username.text, _password.text, _city.text, _country.text, _address.text, _language.text)
    .then((onValue) {
      // saveData(onValue["token"]);
      if (mounted) setState(() => _isLoading = false);
      Navigator.pushReplacementNamed(context, '/channels');
    })
    .catchError((onError) {
      if (mounted) setState(() => _isLoading = false);
      print(onError);
    });
  }

  Widget _showRegisterBtn() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0),
        child: RaisedButton(
          child: Text('Registrarse', style: TextStyle(color: Colors.white),),
          onPressed: register,
          color: Color.fromRGBO(48, 134, 26, 1.0),
        ),
      ),
    );
  }
    
  Widget _showSpinner() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(value: null,),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(top: 180.0, left: 30.0, right: 30.0),
          child: Form(
              key: _formKey,
              child: Column(
              children: <Widget>[
                TextFormField(
                  controller: _username,
                  autovalidate: true,
                  decoration: InputDecoration(
                    hasFloatingPlaceholder: true,
                    labelText: 'Username',
                  ),
                ),
                TextFormField(
                  obscureText: true,
                  controller: _password,
                  autovalidate: true,
                  decoration: InputDecoration(
                    hasFloatingPlaceholder: true,
                    labelText: 'Password',
                  ),
                ),
                TextFormField(
                  controller: _country,
                  autovalidate: true,
                  decoration: InputDecoration(
                    hasFloatingPlaceholder: true,
                    labelText: 'Pais',
                  ),
                ),
                TextFormField(
                  controller: _city,
                  autovalidate: true,
                  decoration: InputDecoration(
                    hasFloatingPlaceholder: true,
                    labelText: 'Ciudad',
                  ),
                ),
                TextFormField(
                  controller: _address,
                  autovalidate: true,
                  decoration: InputDecoration(
                    hasFloatingPlaceholder: true,
                    labelText: 'Dirección',
                  ),
                ),
                TextFormField(
                  controller: _language,
                  autovalidate: true,
                  decoration: InputDecoration(
                    hasFloatingPlaceholder: true,
                    labelText: 'Idioma',
                  ),
                ),
                Row(
                  children: <Widget>[
                    _showRegisterBtn(),
                    _showBtnLogin(),
                  ],
                )
              ],
            ),
          ),
        ),
        _isLoading ? _showSpinner() : Container()
      ],
    );
  }
}