import 'package:flutter/material.dart';
import 'package:flutter_connect/services/auth_service.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

typedef ModeCallback = void Function(String mode);

class LoginPage extends StatefulWidget {
  final ModeCallback changeMode;
  LoginPage({this.changeMode});
  @override
  LoginPageState createState() => new LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  FlutterSecureStorage storage = new FlutterSecureStorage();
  AuthService _authService = new AuthService();
  TextEditingController _username = new TextEditingController(text: '');
  TextEditingController _password = new TextEditingController(text: '');
  final _formKey = new GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  Widget _showSpinner() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(value: null,),
      ),
    );
  }

  void saveData(token) async {
    await storage.write(key: 'token', value: token);
  }
  

  void login() async {
    if (_formKey.currentState.validate()) {
      setState(() => _isLoading = true);
      _authService.login(_username.text, _password.text)
      .then((onValue) {
        if (mounted) setState(() => _isLoading = false);
        if (onValue["token"] != null) {
          saveData(onValue["token"]);
          Navigator.pushReplacementNamed(context, '/channels');
        }
        print(onValue);
      })
      .catchError((onError) {
        if (mounted) setState(() => _isLoading = false);
        print(onError);
      });
    }
  }

  Widget _showBtnLogin() {
    return Padding(
      padding: EdgeInsets.only(top: 20.0),
      child: RaisedButton(
        child: Text('Ingresar', style: TextStyle(color: Colors.white),),
        onPressed: login,
        color: Color.fromRGBO(48, 134, 26, 1.0),
      ),
    );
  }

  Widget _showRegisterBtn() {
    return Padding(
      padding: EdgeInsets.only(top: 20.0),
      child: FlatButton(
        child: Text('Registrarse'),
        onPressed: () => widget.changeMode("register"),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(top: 300.0, left: 30.0, right: 30.0),
          child: Form(
              autovalidate: true,
              key: _formKey,
              child: Column(
              children: <Widget>[
                TextFormField(
                  controller: _username,
                  autovalidate: true,
                  decoration: InputDecoration(
                    hasFloatingPlaceholder: true,
                    labelText: 'Username',
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                  },
                ),
                TextFormField(
                  obscureText: true,
                  controller: _password,
                  autovalidate: true,
                  decoration: InputDecoration(
                    hasFloatingPlaceholder: true,
                    labelText: 'Password',
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    _showBtnLogin(),
                    _showRegisterBtn()
                  ],
                )
              ],
            ),
          ),
        ),
        _isLoading ? _showSpinner() : Container()
      ],
    );
  }
}