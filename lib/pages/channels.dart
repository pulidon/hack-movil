import 'package:flutter/material.dart';
import 'package:flutter_connect/services/channels_service.dart';
import 'package:flutter_connect/pages/messages.dart';

class ChannelsPage extends StatefulWidget {
  ChannelsPageState createState() => ChannelsPageState();
}

class ChannelsPageState extends State<ChannelsPage> {
  ChannelService _channelService = new ChannelService();
  List canales = new List();
  bool _isLoading = false;
  @override
  void initState() {
    super.initState();
    getChannels();
  }

  void getChannels() {
    setState(() => _isLoading = true);
    _channelService.getChannels()
    .then((onValue) {
      if (mounted)  setState(() => _isLoading = false);
      setState(() {
        canales = onValue;
      });
      print(onValue);
    })
    .catchError((onError) {
      if (mounted)  setState(() => _isLoading = false);
      print(onError);
    });
  }


  List _cities =
  ["Español", "Portugues", "Aleman", "Frances"];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentCity;



  Widget _showBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Image.asset(
        'assets/channels_background.png',
        fit: BoxFit.fitHeight,
      ),
    );
  }

   Widget _showDrawer(){
    return Drawer(
    // Add a ListView to the drawer. This ensures the user can scroll
    // through the options in the Drawer if there isn't enough vertical
    // space to fit everything.
    child: ListView(
      // Important: Remove any padding from the ListView.
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          child: Text('Usuario'),
          decoration: BoxDecoration(
            color: Colors.green,
          ),
        ),
        ListTile(
          title: Text('Lenguaje'),
          onTap: () {
            _showDialog();
          },
        ),
      ],
    ),
  );
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _cities) {
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
  }

  void _showDialog() {
  
    showDialog(
      context: context,
      builder: (BuildContext context) {
      
        return AlertDialog(
          title: new Text("¿Desea cambiar de lenguaje?"),
          content: new Container(
            color: Colors.white,
            child:  new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Text("Seleccione su lenguaje"),
              new Container(
                padding: new EdgeInsets.all(16.0),
              ),
              new DropdownButton(
                value: getDropDownMenuItems()[0].value ,
                items: getDropDownMenuItems(),
                onChanged: changedDropDownItem,
              )
            ],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

 void changedDropDownItem(String selectedCity) {
    setState(() {
      _currentCity = selectedCity;
    });
  }

  Widget _showSpinner() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(value: null,),
      ),
    );
  }
 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: _showDrawer(),
      appBar: AppBar(
        title: Text('Canales'),
        centerTitle: true,

      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            _showBackground(),
            ListView.builder(
              itemBuilder: (context, i) {
                 return Card(
                   color: Color.fromRGBO(0, 0, 0, 0.5),
                  child: InkWell(
                    onTap: () {
                      print(canales[i]);
                      // Navigator.pushNamed(context, '/messages');
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MessagesPage(idChannel: canales[i]["id"]))
                      );
                    },
                    splashFactory: InkRipple.splashFactory,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(

                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(left: 15.0),
                              height: 30.0,
                              child: Image.asset(
                                'assets/canal.png',
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 15.0, top: 15.0),
                              child: Text(canales[i]["name"], style: TextStyle(fontWeight: FontWeight.w700, fontSize: 25.0, color: Color.fromRGBO(48, 134, 26, 1)),),
                            ),
                          ],
                        ),

                        Padding(
                          padding: EdgeInsets.only(left: 15.0, right: 15.0),
                          child: Text(canales[i]["topic"], style: TextStyle(fontSize: 13.0, color: Color.fromRGBO(210, 210, 210, 1)),),
                        ),
                        Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Text(canales[i]["description"], style: TextStyle(fontWeight: FontWeight.w100, fontSize: 13.0, color: Color.fromRGBO(210, 210, 210, 1) ),),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: canales.length ,
                 
              
            )
          ],
        ),
      ),
    );
  }
}