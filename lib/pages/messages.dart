import 'package:flutter/material.dart';
import 'package:flutter_connect/services/messages_service.dart';

class MessagesPage extends StatefulWidget {
  final int idChannel;
  MessagesPage({this.idChannel});
  MessagesPageState createState() => MessagesPageState();
}

class MessagesPageState extends State<MessagesPage> {
  MessagesService _messagesService = new MessagesService();
  List messages = new List();
  bool _isLoading = true;

  @override
  initState() {
    super.initState();
    print(widget.idChannel);
    _getMessages(widget.idChannel);
  }

  void _getMessages(int id) {
    setState(() => _isLoading = true);
    _messagesService.getMessagesByChannel(id)
    .then((onValue) {
      if (mounted) setState(() => _isLoading = false);
      setState(() {
        messages = onValue["messages"];
      });
      print(onValue);
    })
    .catchError((onError) {
      if (mounted) setState(() => _isLoading = false);
      print(onError);
    });
  }

  Widget _showBackground() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Image.asset(
        'assets/login_message.png',
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _showInputMessage() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container( 
        padding: EdgeInsets.all(2.0),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(5.0),
                child:  TextField(),                
              ),
            ),
            IconButton(
              icon: Icon(Icons.send),
              onPressed: () {
                print('sent');
              },
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mensajes'),
        centerTitle: true,
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            _showBackground(),
            ListView.builder(
              itemCount: messages.length,
              itemBuilder: (context, i) {
                return Card(
                  color: Color.fromRGBO(0, 0, 0, 0.5),
                  child: InkWell(
                    splashFactory: InkRipple.splashFactory,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(left: 15.0),
                              height: 20.0,
                              child: Image.asset(
                                'assets/16_Message-512.png',
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 15.0, top: 15.0),
                              child: Text(messages[i]["usuario"], style: TextStyle(fontWeight: FontWeight.w700, fontSize: 25.0, color: Color.fromRGBO(48, 134, 26, 1)),),
                            ),
                          ],
                        ),

                        Padding(
                          padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0, top: 15.0),
                          child: Text(messages[i]["mensaje"], style: TextStyle(fontSize: 16.0, color: Color.fromRGBO(210, 210, 210, 1)),),
                        ),

                      ],
                    ),
                  ),
                );
              },
            ),
            _showInputMessage()
          ],
        ),
      ),
    );
  }
}