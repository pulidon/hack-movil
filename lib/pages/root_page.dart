import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_connect/pages/login_page.dart';
import 'package:flutter_connect/pages/register_page.dart';

class RootPage extends StatefulWidget {

  @override
  RootPageState createState() => new RootPageState();
}

class RootPageState extends State<RootPage> {
  String _mode = "register";

  @override
  initState() {
    super.initState();
  }

  Widget _showBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Image.asset(
        'assets/login_background.png',
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _showTitle() {
    return Container(
      padding: EdgeInsets.all(35.0),
      height: 250.0,
      child: Image.asset('assets/titulo.png'),
    );
  }

  _changeMode(String newMode) {
     print(newMode);
    setState(() {
      _mode = newMode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              _showBackground(),
              _showTitle(),
              Container(
                height: MediaQuery.of(context).size.height,
                child:  _mode == "login" ? LoginPage(changeMode: (o) => _changeMode(o),) : RegisterPage(changeMode: (String e) => _changeMode(e),),
              ),
            ],
          )
        ],
      ),
    );
  }
}